static import std;

import core.thread:Fiber,Thread, dur;
import core.time:MonoTime;

import std.algorithm, std.array;
import std.conv: to;
import std.range:chain;

import ncurses = deimos.ncurses;

import entity;

class Controller {
    Entity[] entities;
    bool running = true;
    static Controller controller;
    ncurses.WINDOW* win;
    static private int ch;;
    private Fiber read;
    private Fiber draw;
    private Fiber next;
    this(Entity[] entities){
        this.entities = entities;
        controller = this;

        read = new Fiber(&_read);
        draw = new Fiber(&_draw);
    }

    void run(){
	    win = ncurses.initscr;
        ncurses.curs_set(0);
        ncurses.noecho();
        ncurses.timeout(0);
        ncurses.keypad(ncurses.stdscr, true);
        
        while(running){
            auto start = MonoTime.currTime;
            runFrame;
            if (ch ==27) Controller.controller.running = false;
            entities.each!(e=>e.next);
            collizion;
            purgeDestroyed;
            auto end = MonoTime.currTime;
            ncurses.refresh;
            auto frametime = 50.dur!"msecs" - (end-start).to!(std.Duration);
            if(!frametime.isNegative)
                Thread.sleep(frametime);
            ncurses.clear;
        }
        ncurses.endwin;
    }  

    static auto getch(){
        return ch;
    }

    void purgeDestroyed(){
        Entity[] filtered = [];
        entities.filter!(e => !e.destroyed ).each!(e => filtered~=e);
        entities = filtered;
    }
    
    void collizion(){
        foreach(i;0..entities.length)
            foreach(j;0..entities.length)
                    if(i!=j && (entities[i] & entities[j]) &&
                        !areFriends(entities[i], entities[j])    ) {
                        entities[i].destroyed = true;
                        entities[j].destroyed = true;
                    }
    }
    bool areFriends(Entity a, Entity b){
        return a.friend is b.friend ||
               a is b.friend ||  
               b is a.friend ;
    }

    auto drawLogs() {
            string tmp = entities.map!(e => e.to!string).array.to!string;
            (cast(char*) tmp.ptr)[tmp.length]=0;
            ncurses.mvprintw(0,0,tmp.ptr);
    }

    private auto runFrame(){
        if(read.termed) read.reset;
        if(draw.termed) draw.reset;
        while(read.holded|| draw.holded){
            if(read.holded) read.call;
            if(draw.holded) draw.call;
            ncurses.refresh;
        }
    }

    private void _read(){
        int _ch;
        do{
            _ch = ncurses.getch;
            Fiber.yield;
        }while(_ch == -1 && !draw.termed);
        this.ch = _ch;
    }

    private void _draw(){
        foreach(e; entities){
            e.draw(win);
            Fiber.yield;
        }
        drawLogs;
    }

    unittest {
        import plane;
        auto p = new Plane(Point(10,10));
        auto c = new Controller([p]);
        assert (c.entities.length == 1, "Shoul be 1 plane");
        foreach (_;0..9) p.fire;
        assert (c.entities.length == 10, 
                "Shoul be 1 plane and 9 bullets, but "~
                c.entities.length.to!string);
        foreach (_;0..10)c.entities.each!(e => e.next);
        assert (c.entities.length == 10, 
                "Shoul be 1 plane and 9 bullets before purge");
        c.purgeDestroyed;
        assert (c.entities.length == 1, "Shoul be 1 plane after purge"
                ~c.entities.to!string);


    }

}


bool holded(Fiber t){
    return t.state == Fiber.State.HOLD;
}
bool termed(Fiber t){
    return t.state == Fiber.State.TERM;
}
