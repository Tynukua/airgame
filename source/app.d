import std.container:SList;
import std.stdio;


import entity, plane, enemy;
import controller;


void main()
{   
    Entity[] entities= [new Plane(Point(30,50))];
    entities ~= Enemy.spawn;
    auto contoller = new Controller(entities);
    contoller.run;    
}

