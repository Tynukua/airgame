import std.format:format;
struct Point {
    int x;
    int y;

    Point opBinary(string op)(Point r){
        Point answer;
        static foreach(f; ["x", "y"]){
            mixin("answer.%s = %s %s r.%s;".format(f, f, op, f));
        }
        return answer;
    }
}

unittest {
    assert(Point(1,2)+Point(2,1) == Point(3, 3));
    assert(Point(2,-2)+Point(2,1) == Point(4, -1));
    
    assert(Point(1,2)-Point(2,1) == Point(-1, 1));
    
    assert(Point(2,-2)*Point(2,1) == Point(4, -2));
    assert(Point(2,-2)*Point(3,1) == Point(6, -2));
    
    assert(Point(6,-3)<<Point(3,1) == Point(6<<3, -3 <<1));
    assert(Point(6,-3)>>Point(10,1) == Point(6 >> 10, -3 >>1));
}
