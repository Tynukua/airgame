import std.stdio;
import std.array;
import std.algorithm;
import std.conv: to;

char[][] loadTexture(T)(T source, char nullsymbol = ' ') {
    char[] texture;
    static if ( is(T == File)) {
        texture= new char[source.size];
        source.rawRead(texture);
    }else static if(is(T==string) ){
       texture = source.to!(char[]); 

    }
    else static assert(0);
    texture.each!((ref c) => c = (c==nullsymbol?0:c)) ;
    return texture.split;
}

unittest{
    "loadTexture test".write;
    assert(loadTexture(" | ") == [['\0','|','\0']] );
    enum testTexture = ["\0;)\0",
                        ":-("
    ].to!(char[][]);
    auto actually = loadTexture(File("views/testTexture.txt"));
    assert( actually == testTexture, testTexture.to!string ~`!=`~actually.to!string);
    " passed".writeln;
}

