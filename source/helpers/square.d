import point;

import std.stdio;

struct Square {
    Point a;
    Point b;

    this(Point _a, Point _b){
        a = _a; 
        b = _b;
    }

    this(int ax, int ay, int bx, int by){
        a = Point(ax, ay);
        b = Point(bx, by);
    }

    Point[4] corners() {
        Point c=a, d=a;
        c.x = b.x;
        d.y = b.y;
        return [a, c, d, b];
    }

    bool opBinaryRight(string op)(Point i){ 
        static if (op == "in")
        {
            return a.x <= i.x && i.x <= b.x &&  
                   a.y <= i.y && i.y <= b.y; 
        }
        else static if (op == "!in")
        {
            return !(i in this);
        }
        else static assert(0);
    }

    auto opBinary(string op)(Square another){
        static if(op=="&"){
            foreach (p; corners){
                if(p in another) {
                    return true;
                }
            }
            return false;
                  
                  
                  
        }
        else static assert(0);
    }

    unittest {
        "Point in Square".write;
        assert(Point(3,4) !in Square(5,6, 10,11), "Shouldn't be in Square");
        assert(Point(7,4) !in Square(5,6, 10,11), "Shouldn't be in Square");
        assert(Point(7,7) in Square(5,6, 10,11), "Should be in Square");
        " passed".writeln;
    }

    unittest {
        "Corners generator".write;
        assert([1,2,3]==[1,2,3], "Should array checking");
        assert(
                Square(1,1,2,3).corners == [
                        Point(1,1),
                        Point(2,1),
                        Point(1,3),
                        Point(2,3)], 
                "Point Checks"
        );
        " passed".writeln;
    }

    unittest {
        "Square collizion".write;
        assert(!(Square(1,2, 3,4)& Square(5,6, 10,11)), 
                "Should be not collized");
        assert( Square(1,2, 3,4)& Square(3,4, 10,11), "Should be collized");
        assert(!(Square(5,6, 10,11)& Square(1,2, 3,4)), 
                "Should be not collized");
        assert( Square(3,4, 10,11)& Square(1,2, 3,4), "Should be collized");
        " passed".writeln;
    }
}
