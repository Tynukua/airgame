import std.string;
import std.conv:to;

import ncurses = deimos.ncurses;

import entity, bullet;
import controller;

class Plane: Entity {
    this(
            Point p, 
            char[][] texture = import("plane.txt").loadTexture)
    {
        super(p, texture);
        friend = this;
    }
    override void next(){
        int entered = Controller.getch;
        switch(entered){
            default: break;
            case 'w': position.y--;break;
            case 's': position.y++;break;
            case 'd': position.x++;break;
            case 'a': position.x--;break;
            case 'W': position.y-=2;break;
            case 'S': position.y+=2;break;
            case 'D': position.x+=2;break;
            case 'A': position.x-=2;break;
            case ' ': fire;break;
        }
    }
    void fire(){
        auto ptexture = texture;
        auto pposition = position;
        auto psize = size;
        auto plane = this;
        Controller.controller.entities ~= new Bullet(
                position - Point(2, size.y/2), 
                Point(0,-1),
                ["^","|"].to!(char[][]),
                this);
    }
    override string toString(){
        return "Plane " ~ super.toString;
    }

}

