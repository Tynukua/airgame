import std.math:abs;
import std.array:split, array;
import std.conv:to;
import std.random:uniform;
import std.algorithm;
import std.math:sgn;
import ncurses = deimos.ncurses;

import entity, bullet;
import controller;

class Enemy: Entity {
    int yState;
    int yDirection = 1;
    int lastXDirection = 1;
    static Enemy[] roi = [];
    static int direction = 1;   
    int gunCooldown = 0;
    enum COOLDOWN = 60;
    this(
            Point p, 
            char[][] texture = import("enemy.txt").loadTexture)
    {
        roi ~= this;
        super(p, texture);
        friend = this;
        yState = uniform!"[]"(-2,2);
    }

    override void next(){
        enum AMPLITUDE = 3;
        if (isBordered){
            direction =- direction;
            roi.each!(e => e.position.y++);
        }
        position.x += direction;
        
        if (yState.abs >= AMPLITUDE) 
            yDirection =- yDirection;
        yState += yDirection;
        position.y += yDirection;
        
        if (position.y - 5 > ncurses.LINES)
            Controller.controller.running = false;
        fire;   
    }
    void fire(){
        if(gunCooldown == 0){
            Point v = Controller.controller.entities[0].position - position;
            v.x = v.x.sgn;
            v.y = 1; 
            Controller.controller.entities ~= new Bullet(
                    position, v, [['*']], this.friend );
            gunCooldown = COOLDOWN;
            return;
        }
        gunCooldown--; 
    }

    static Entities spawn(){
        int x=5, y =12;
        if (roi.length>=0)cleanRoi;
        Entities enemies = [new Enemy(Point(x,y))];
        foreach (_;0..10){
            x+=12;
            y += uniform!"[]"(-1,1);
            auto t = new Enemy(Point(x,y));
            t.gunCooldown = uniform(COOLDOWN/2,COOLDOWN);
            t.friend = enemies[0];
            enemies~= t;
        }
        return enemies;
    }

    bool atBorder(){
        return ((position.x  <= 0 && direction == -1) ||
                (position.x >= ncurses.COLS && direction == 1));
    }
    
    static bool isBordered(){
        foreach(e; roi.filter!(e => !e.destroyed))
            if (e.atBorder)return true;
        return false;
    }

    static cleanRoi()
    {
        roi = roi.filter!(e => !e.destroyed).array;
    }

}

