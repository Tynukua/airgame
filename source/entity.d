import std.stdio;
import std.conv: to;
import std.algorithm, std.array;
import core.exception;
import deimos.ncurses;

public import helpers;

alias Entities = Entity[];

abstract class Entity {
    Point position;
    Point size;
    char[][] texture;
    bool destroyed = false;
   
    Entity friend;

    this(Point p, char[][] texture)
    in(texture !is null)
    {
        position = p;
        this.texture = texture;
        size = Point( 
                texture.map!(r => r.length).maxElement.to!int,
                texture.length.to!int
                );      
    }

    void draw(WINDOW* win)
    in(win !is null)
    {
        foreach(p,c; drawMap){
            win.mvwaddch(p.y,p.x,c);
        }
    }
    Square range() {
        auto s = Point(position.x-size.x/2,position.y-size.y/2);
        
        return Square(s, Point(s.x +size.x, s.y+size.y));
    }
    
    override string toString(){
        import std.format;
        
        return "C[%d;%d ".format(position.x, position.y) ~ size.to!string;
    }

    int[Point] drawMap(){
        int[Point] _map;
        foreach (i,row; texture){
            foreach(j, c; row){
                int x = position.x + cast(int)j - size.x/2;
                int y = position.y + cast(int)i - size.y/2;
                if(c!=0)
                    _map[Point(x,y)] = c;
            }
        }
        return _map;
    }

    auto opBinary(string op)(Entity e){
         static if(op == "&"){
            auto eDrawMap = e.drawMap;
            foreach(k, v; drawMap){
                if(k in eDrawMap){
                    return true;
                }
            } 
            return false;
         }
    }

    abstract void next();
}


unittest {
    class EntityMock: Entity {
        this(Point p){
            super(p,import("plane.txt").loadTexture);
        }
        override void next(){}
    }
    auto a = new EntityMock(Point(0,0));
    auto b = new EntityMock(Point(16,8));
    assert(a.range & b.range, 
            "range should be crossed" ~ a.range.to!string ~ b.range.to!string);
    assert(! (a & b), "shoud be not crossed");
    
    auto c = new EntityMock(Point(1,1));
    assert(a.range & c.range, 
            "range should be crossed" ~ a.range.to!string ~ c.range.to!string);
    assert((a & c), "shoud becrossed"~ a.range.to!string ~ c.range.to!string);
}
