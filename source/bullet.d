import std.conv:to;
import std.math:sgn;

import deimos.ncurses;

import entity;

class Bullet: Entity{
    Point speed;
    Point step;
    this(
            Point start, 
            Point speed, 
            char[][] texture = [['|']], 
            Entity shooter = null){
        super(start, texture);
        friend = shooter;
        this.speed = speed;
        step = speed;
    }

    override string toString(){
        return super.toString ~ step.to!string ~ speed.to!string;
    }
    override void next() {
        if(step == Point(0,0)){
            step = speed;
        }
        if (step.x != 0 ){
            position.x += step.x.sgn;
            step.x -= step.x.sgn;
        }
        else if (step.y!=0){
            position.y += step.y.sgn;
            step.y -= step.y.sgn;
        }

        if (position.x > COLS || position.x<0)
            destroyed = true;
        if (position.y > LINES|| position.y<0)
            destroyed = true;
    }
}
